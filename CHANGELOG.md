# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

## [1.0.3] - 2020-07-13

### Changed
- Docker image based on Alpine Linux and NGINX 1.19.1.
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.

## [1.0.2] - 2020-06-03

### Changed
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.
- Docker image based on Alpine Linux and NGINX 1.19.0.

## [1.0.1] - 2020-05-27

### Changed
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.

## [1.0.0] - 2020-05-03

### Added
- Project readme.
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Continuous Integration.
- Default root directory.
- NGINX configuration file.
- Remove unnecessary files from Docker build context.
- Test script.
- Docker image based on Alpine Linux and NGINX 1.18.0.
- Project license.
- Contributing guide.
- Record of all notable changes made to this project.
