FROM \
    nginx:1.19.1-alpine
WORKDIR \
    /var/www
RUN \
    mkdir \
        -p \
            /var/www
COPY \
    /filesystem/ \
    /
CMD \
    start
